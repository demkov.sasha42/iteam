interface IBreed {
  name: string,
  description: string
}

export interface ICatResponse {
  breeds: IBreed[],
  id: string,
  url: string
}

export interface ICat {
  breed: string,
  id: string,
  url: string
  description: string
}
