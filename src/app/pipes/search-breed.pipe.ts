import { Pipe, PipeTransform } from '@angular/core';
import {ICat} from "../interfaces/icat";

@Pipe({
  name: 'searchBreed'
})
export class SearchBreedPipe implements PipeTransform {

  transform(cats: ICat[], input: string): ICat[] {
    return cats.filter((cat: ICat) => cat.breed.toLowerCase().includes(input.toLowerCase()));
  }

}
