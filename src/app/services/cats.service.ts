import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ICatResponse} from "../interfaces/icat";

@Injectable({
  providedIn: 'root'
})
export class CatsService {

  apiKey: string = 'x-api-key';
  apiValue: string = 'live_rVjzavhLnG6tMJ9DkxSbidsgLimbCHglDVOBwp62VGXUIumNeyIwmfnc5EatYt1k';
  private $HOST: string = 'https://api.thecatapi.com/v1/images/search?limit=100&has_breeds=1'

  constructor(private http: HttpClient) { }

  getCats(): Observable<ICatResponse[]> {
    return this.http.get<ICatResponse[]>(this.$HOST)
  }
}
