import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {CatsService} from "../services/cats.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private catsService: CatsService) {}

  intercept(request: HttpRequest<XMLHttpRequest>, next: HttpHandler): Observable<HttpEvent<XMLHttpRequest>> {
    request = request.clone({
      setHeaders: {
        [this.catsService.apiKey]: this.catsService.apiValue
      }
    })
    return next.handle(request);
  }
}
