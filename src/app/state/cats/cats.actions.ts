import {createAction, props} from "@ngrx/store";
import {ICatResponse} from "../../interfaces/icat";

export const addLoadCats = createAction('App Load Cats');

export const addLoadCatsSuccess = createAction('Add Load Cats Success',
  props<{cats: ICatResponse[]}>()
)

export const addLoadCatsError = createAction('Add Load Cats Error',
  props<{error: Error}>()
)
