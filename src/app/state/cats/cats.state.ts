import {ICat} from "../../interfaces/icat";

export interface CatsState {
  cats: ICat[]
}

export const initialStateCats: CatsState = {
  cats: []
}
