import {CatsService} from "../../services/cats.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Injectable} from "@angular/core";
import {addLoadCats, addLoadCatsError, addLoadCatsSuccess} from "./cats.actions";
import {catchError, map, switchMap, of} from "rxjs";

@Injectable()
export class CatsEffects {

  constructor(private catsService: CatsService,
              private actions$: Actions) {
  }

  getCats$ = createEffect(() => this.actions$.pipe(
    ofType(addLoadCats),
    switchMap(() => this.catsService.getCats().pipe(
      map(cats => addLoadCatsSuccess({cats})),
      catchError(error => of(addLoadCatsError({error})))
    ))
  ))

}
