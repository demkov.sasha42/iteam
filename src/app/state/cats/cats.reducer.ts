import {createReducer, on} from "@ngrx/store";
import {initialStateCats} from "./cats.state";
import {addLoadCatsSuccess} from "./cats.actions";
import {ICat} from "../../interfaces/icat";

export const catsReducer = createReducer(
  initialStateCats,
  on(addLoadCatsSuccess, (state, {cats}) => {
    const transformCats: ICat[] = [];
    cats.forEach(cat => {
      if (cat.breeds[0]) {
        transformCats.push({
          id: cat.id,
          url: cat.url,
          breed: cat.breeds[0].name,
          description: cat.breeds[0].description})
        }
    })
    return {cats: transformCats}
  })
)
