import {Component, Input, OnInit} from '@angular/core';
import {ICat} from "../../../interfaces/icat";

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.css']
})
export class CatComponent implements OnInit {

  textSubtitle: string = 'cat breed';
  @Input() cat: ICat;

  constructor() { }

  ngOnInit(): void {
  }

}
