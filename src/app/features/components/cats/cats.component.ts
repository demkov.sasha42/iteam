import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ICat} from "../../../interfaces/icat";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.css']
})
export class CatsComponent implements OnInit {

  @Input() cats: ICat[];
  searchValue: string = '';
  count: string;
  pageEvent: PageEvent;
  currentCatsLength: number = 100;
  @ViewChild('paginator') paginator: MatPaginator

  constructor() { }

  ngOnInit(): void {}

  getSearchCatInput(formValue: {search: string}) {
    this.searchValue = formValue.search;
    const currentCats: ICat[] = this.cats.filter((cat: ICat) => {
      return cat.breed.toLowerCase().includes(formValue.search.toLowerCase())
    });
    this.currentCatsLength = currentCats.length;
    this.paginator.pageIndex = 0;
  }

}
