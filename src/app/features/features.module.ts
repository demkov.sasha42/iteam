import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatsComponent } from './components/cats/cats.component';
import { CatComponent } from './components/cat/cat.component';
import {MaterialModule} from "../material/material.module";
import {SharedModule} from "../shared/shared.module";
import {SearchBreedPipe} from "../pipes/search-breed.pipe";



@NgModule({
  declarations: [
    CatsComponent,
    CatComponent,
    SearchBreedPipe
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule
  ],
  exports: [
    CatsComponent
  ]
})
export class FeaturesModule { }
