import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ISearch} from "../../../interfaces/isearch";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})

export class ToolbarComponent implements OnInit {

  labelSearch: string = 'Input cat breed';
  formSearch: FormGroup;
  @Output() searchEmitter: EventEmitter<ISearch> = new EventEmitter<ISearch>();

  constructor() { }

  ngOnInit(): void {
    this.formSearch = new FormGroup({
      search: new FormControl
    })
  }

}
