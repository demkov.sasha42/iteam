import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "./interceptors/token.interceptor";
import {StoreModule} from "@ngrx/store";
import {catsReducer} from "./state/cats/cats.reducer";
import {EffectsModule} from "@ngrx/effects";
import {CatsEffects} from "./state/cats/cats.effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {environment} from "../environments/environment";
import {CoreModule} from "./core/core.module";
import {FeaturesModule} from "./features/features.module";
import {MaterialModule} from "./material/material.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({cats: catsReducer}),
    EffectsModule.forRoot([CatsEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
      autoPause: true,
    }),
    CoreModule,
    FeaturesModule,
    MaterialModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    multi: true,
    useClass: TokenInterceptor
  }],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
