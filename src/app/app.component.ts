import {Component, OnInit} from '@angular/core';
import {CatsService} from "./services/cats.service";
import {ICat} from "./interfaces/icat";
import {Store} from "@ngrx/store";
import {addLoadCats} from "./state/cats/cats.actions";
import {selectCatsData} from "./state/cats/cats.selector";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  cats: ICat[];

  constructor(private catsService: CatsService,
              private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch(addLoadCats());
    this.store.select(selectCatsData).subscribe(data => {
      this.cats = data;
    })
  }

}
